import uuid

class AnalysisAlarm(object):
    def __init__(self, item, event):
        self.item = item
        self.event = event

        self.AlarmType = {}
        self.AlarmType['elb'] = self.elb_alarm
        self.AlarmType['ec2'] = self.ec2_alarm
        # self.AlarmType['rds'] = self.rds_alarm

    def elb_alarm(self):
        log = {
            'id': str(uuid.uuid4()),
            **{key: val for key, val in self.item.items()},
            'time': self.event['time'],
            'value': self.event['detail']['state']['value'],
            'target_group': self.event['detail']['configuration']['metrics'][0]['metricStat']['metric']['dimensions']['TargetGroup'],
            'load_balancer': self.event['detail']['configuration']['metrics'][0]['metricStat']['metric']['dimensions']['LoadBalancer'],
        }

        return log

    def ec2_alarm(self):
        log = {
            'id': str(uuid.uuid4()),
            **{key: val for key, val in self.item.items()},
            'time': self.event['time'],
            'value': self.event['detail']['state']['value'],
            'instance_id': self.event['detail']['configuration']['metrics'][0]['metricStat']['metric']['dimensions']['InstanceId']
        }

        return log

    def result(self):
        return self.AlarmType[self.item['alarm_type']]()


class AnalysisMetrics(object):
    def __init__(self, alarm_type, event):
        self.alarm_type = alarm_type
        self.event = event

        self.AlarmType = {}
        self.AlarmType['elb'] = self.elb_image
        self.AlarmType['ec2'] = self.ec2_image
        # self.AlarmType['rds'] = self.rds_image

    def elb_image(self):
        metrics = """{
            "metrics": [
                [ "AWS/ApplicationELB", "HealthyHostCount", "TargetGroup", "targetgroup/season-tg/e67622a6b5cd2660", "LoadBalancer", "app/season-lb/4f036cd2f35dec14", "AvailabilityZone", "us-west-1a", { "stat": "Average" } ]
            ],
            "view": "timeSeries",
            "stacked": false,
            "period": 60,
            "annotations": {
                "horizontal": [
                    {
                        "label": "HealthyHostCount < 1 for 1 datapoints within 1 minute",
                        "value": 1
                    }
                ]
            },
            "title": "season-alarm-host-count",
            "width": 1309,
            "height": 250,
            "start": "-PT3H",
            "end": "P0D",
            "timezone": "+0800"
        }"""

        return metrics

    def ec2_image(self):
        pass

    def result(self):
        return self.AlarmType[self.alarm_type]()
