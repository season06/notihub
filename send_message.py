import json
import time

import urllib3
from urllib.parse import urlencode
from urllib.request import Request, urlopen

import slack
from slack import WebClient
from slack.errors import SlackApiError

from msg_template import slack, teams


class SendMessage(object):
    def __init__(self, info, status):
        self.info = info
        self.status = status

        # getattr(self, status)()

        self.PLATFORM = {}
        self.PLATFORM['slack'] = self.slack
        self.PLATFORM['teams'] = self.teams
        # self.PLATFORM['whatsapp'] = self.whatsapp

    def slack(self):
        content = slack.SlackTemplate(self.info, self.status).result()

        try:
            client = WebClient(token=self.info['api_token'])

            result = {
                'channel': '#test',
                'blocks': content
            }
            if self.status == 'cloudwatch_event':
                result['attachments'] = result.pop('blocks')

            response = client.chat_postMessage(
                **result
            )
            print(f'send message success - slack - {self.status}')

        except SlackApiError as e:
            raise Exception(e.response["error"])

    def teams(self):
        content = teams.TeamsTemplate(self.info, self.status).result()

        try:
            http = urllib3.PoolManager()
            r = http.request("POST",
                             self.info['api_token'],
                             body=json.dumps(content),
                             headers={"Content-Type": "appilcation/json"})
            print(f'send message success - teams - {self.status}')

        except Exception as e:
            raise Exception(e)

    def result(self):
        return self.PLATFORM[self.info['platform']]()


def send_to_msp(api_token, content):
    # platform: email
    client = WebClient(token=api_token)

    try:
        response = client.chat_postMessage(
            channel="#test",
            text=content
        )

    except SlackApiError as e:
        raise Exception(e.response["error"])
