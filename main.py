import json, time, pytz, base64
import traceback
from http import HTTPStatus
from datetime import datetime, timedelta, timezone

import requests
import pyotp
import boto3
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Key, Attr

from analysis_alarm import AnalysisAlarm
from do_action import DoAction
from send_message import SendMessage, send_to_msp
from cloudwatch_image import cloudwatch_image

SECRET_NAME = "notihub"
REGION = "us-west-1"
ROLE_ARN = "arn:aws:iam::863936362823:role/season-account-role"
ROLE_SESSION_NANE = "season.wang"
MSP_EMAIL = ""
S3_BUCkET = "notihub-bucket"

def get_secret_manager():
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=REGION
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=SECRET_NAME
        )
    except ClientError as e:
        raise Exception(e)
    else:
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
            return json.loads(secret)
        else:
            decoded_binary_secret = base64.b64decode(
                get_secret_value_response['SecretBinary'])
            return json.loads(decoded_binary_secret)


def switch_role():
    secret_param = get_secret_manager()

    # get mfa code
    time_remaining = 0
    while time_remaining < 2:
        totp = pyotp.TOTP(secret_param['mfa_token'])
        time_remaining = totp.interval - datetime.now().timestamp() % totp.interval
        mfa_code = totp.now()

    sts_client = boto3.client(
        'sts',
        aws_access_key_id=secret_param['aws_access_key_id'],
        aws_secret_access_key=secret_param['aws_secret_access_key']
    )

    # switch role
    assumed_role_object = sts_client.assume_role(
        RoleArn=ROLE_ARN,
        RoleSessionName=ROLE_SESSION_NANE,
        SerialNumber=secret_param['mfa_role_arn'],
        TokenCode=mfa_code
    )

    return assumed_role_object['Credentials']


def get_alarm(event):
    info = {
        'account_id': event['account'],
        'region': event['region'],
        'alarm_name': event['detail']['alarmName'],
    }

    secret_param = get_secret_manager()
    
    dynamodb = boto3.resource(
        'dynamodb',
        region_name='us-west-1',
        aws_access_key_id=secret_param['aws_access_key_id'],
        aws_secret_access_key=secret_param['aws_secret_access_key']
    )
    table = dynamodb.Table('notihub')
    log_table = dynamodb.Table('notihub_log')
    prtg_table = dynamodb.Table('notihub_prtg')

    try:
        # check prtg table
        response = prtg_table.get_item(Key={'account_id': info['account_id']})
        item = response.get('Item')
        if item:
            response = prtg_table.update_item(
                Key={
                    'account_id': info['account_id'],
                },
                UpdateExpression="set stage=:s",
                ExpressionAttributeValues={
                    ':s': 'cloudwatch'
                },
                ReturnValues="UPDATED_NEW"
            )
            print('update stage to `cloudwatch` success')

        # get db info
        response = table.query(
            IndexName='account_id-index',
            KeyConditionExpression=Key('account_id').eq(info['account_id']),
            FilterExpression=Attr('region').eq(info['region']) &
            Attr('alarm_name').eq(info['alarm_name']),
        )

        # alarm not in database, send msg to msp, then BREAK
        if response['Count'] == 0:
            send_to_msp(MSP_EMAIL, event)
            raise Exception('Alarm not in database. Please solve it manually.')

        item = response['Items'][0]  # primary

        # is in disable time?
        if item['is_disable'] == 'true':
            tz = pytz.timezone('Asia/Taipei')
            start_unix = time.mktime(datetime.strptime(item['disable_start_time'], "%Y-%m-%d %H:%M:%S").timetuple())
            end_unix = time.mktime(datetime.strptime(item['disable_end_time'], "%Y-%m-%d %H:%M:%S").timetuple())
            now_unix = time.mktime(datetime.now(tz).timetuple())

            if start_unix < now_unix and now_unix < end_unix:
                raise Exception('The system is under maintenance.')

        # analysis alarm according to type
        log = AnalysisAlarm(item, event).result()

        # put log to db
        log_table.put_item(Item=log)

        # if alarm is solved, send cloudwatch image to client, then BREAK
        if log['value'] == 'OK':
            credentials = switch_role()
            cloudwatch_image(credentials, secret_param, log, event)
            SendMessage(item, 'image').result()
            raise Exception('CloudWatch status turn to OK, send image.')

        # Send msg -> checking
        SendMessage(log, 'cloudwatch_event').result()
        SendMessage(item, 'checking').result()

        return log

    except ClientError as e:
        raise Exception(e.response['Error']['Message'])


def verify_status(credentials, info):
    # elb_client = boto3.client(
    #     'elbv2',
    #     region_name=info['region'],
    #     aws_access_key_id=credentials['AccessKeyId'],
    #     aws_secret_access_key=credentials['SecretAccessKey'],
    #     aws_session_token=credentials['SessionToken'],
    # )

    # response = elb_client.describe_load_balancers(
    #     LoadBalancerArns=[
    #         f"arn:aws:elasticloadbalancing:{info['region']}:{info['account_id']}:loadbalancer/{info['load_balancer']}"]
    # )

    # DNS_name = response['LoadBalancers'][0]['DNSName']
    # r = requests.get(f'http://{DNS_name}')

    r = requests.get(info['url'])

    print(r.status_code)

    is_success = False
    if r.status_code == HTTPStatus.OK.value:
        is_success = True

    return is_success


def main():
    try:
        info = get_alarm(event)

        credentials = switch_role()

        DoAction(credentials, info).result()

        is_success = verify_status(credentials, info) # result store in db?

        if is_success:
            SendMessage(info, 'verify').result()
        else:
            send_to_msp(MSP_EMAIL, event)
            raise Exception('Verify fail. Send msg to MSP.')
    except Exception as e:
        print(e)
        print(traceback.format_exc())

main()
