import boto3
from botocore.exceptions import ClientError

from analysis_alarm import AnalysisMetrics

def cloudwatch_image(credentials, secret_param, info, event):
    # get image metrics
    metrics = AnalysisMetrics(info['alarm_type'], event).result()

    client = boto3.client(
        'cloudwatch',
        region_name=info['region'],
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )

    # output of image is binary
    response = client.get_metric_widget_image(
        MetricWidget=metrics
    )

    client = boto3.client(
        's3',
        aws_access_key_id=secret_param['aws_access_key_id'],
        aws_secret_access_key=secret_param['aws_secret_access_key']
    )

    # push to s3 bucket
    response = client.put_object(
        Bucket='notihub-bucket',  # bucket name
        ContentType='image/png',
        Body=response['MetricWidgetImage'], # my file
        Key='cloudwatch_image/graph.png'  # bucket path
    )
