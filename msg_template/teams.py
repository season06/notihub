class TeamsTemplate(object):
    def __init__(self, info, status):
        self.info = info
        self.status = status

        self.STAGE = {}
        self.STAGE['cloudwatch_event'] = self.cloudwatch_event
        self.STAGE['checking'] = self.checking
        self.STAGE['verify'] = self.verify
        self.STAGE['image'] = self.image

    def cloudwatch_event(self):
        color = '#FF0000' if self.info['value'].upper() == 'ALARM' else '#008000'

        content = {
            "summary": "Card \"Test card\"",
            "themeColor": color,
            "title": f"AWS CloudWatch Notification - {self.info['value'].upper()}",
            "sections": [
                {
                    "activityTitle": "**Alarm Name**",
                    "activitySubtitle": self.info['alarm_name'],
                    "activityImage": "https://haochenbucket.s3.us-east-2.amazonaws.com/istockphoto-484011955-1024x1024.jpeg",
                    "facts": [
                        {
                            "name": "Account:",
                            "value": self.info['account_id']
                        },
                        {
                            "name": "InstanceID:",
                            "value": self.info['instance_id']
                        },
                        {
                            "name": "region:",
                            "value": self.info['region']
                        },
                        {
                            "name": "Status:",
                            "value": self.info['value']
                        },
                        {
                            "name": "Time:",
                            "value": self.info['time']
                        }
                    ],

                }
            ]
        }
        return content

    def checking(self):
        content = {
            "summary": "Card \"Test card\"",
            "themeColor": "#FFFF00",
            "title": 'MSP checking',
            "text": "We are checking currently."
        }
        return content

    def verify(self):
        content = {
            "summary": "Card \"Test card\"",
            "themeColor": "#008000",
            "title": 'Verify Success',
            "text": "request return status - 200"
        }
        return content
    
    def image(self):
        pass

    def result(self):
        return self.STAGE[self.status]()
