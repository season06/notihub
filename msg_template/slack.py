class SlackTemplate(object):
    def __init__(self, info, status):
        self.info = info
        self.status = status

        self.STAGE = {}
        self.STAGE['cloudwatch_event'] = self.cloudwatch_event
        self.STAGE['checking'] = self.checking
        self.STAGE['verify'] = self.verify
        self.STAGE['image'] = self.image

    def cloudwatch_event(self):
        color = 'danger' if self.info['value'].upper() == 'ALARM' else 'good'
        
        content = [
            {
                'title': f"AWS CloudWatch Notification - {self.info['value'].upper()}",
                'color': color,
                'fields': [
                    {
                        'title': 'Account',
                        'value': self.info['account_id'],
                        "short": True
                    },
                    {
                        "title": "Client",
                        "value": self.info['client_name'],
                        "short": True
                    },
                    {
                        "title": "Alarm Name",
                        "value": self.info['alarm_name'],
                        "short": True
                    },
                    {
                        "title": "Value",
                        "value": self.info['value'],
                        "short": True
                    },
                    {
                        "title": "Region",
                        "value": self.info['region'],
                        "short": True
                    },
                    {
                        "title": "Time",
                        "value": self.info['time'],
                        "short": True
                    }
                ]
            }
        ]

        return content

    def checking(self):
        content = [
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": "MSP Checking"
                }
            },
            {
                "type": "section",
                "fields": [
                    {
                        "type": "mrkdwn",
                        "text": f"We are checking currently."
                    }
                ]
            }
        ]
        return content

    def verify(self): # send website screenshot
        content = [
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": "Verify Success"
                }
            },
            {
                "type": "section",
                "fields": [
                    {
                        "type": "mrkdwn",
                        "text": "request return status - 200"
                    }
                ]
            }
        ]
        return content

    def image(self):
        content = [
            {
                "type": "image",
                "title": {
                    "type": "plain_text",
                    "text": "cloudwatch image"
                },
                "image_url": "https://season-bucket.s3-us-west-1.amazonaws.com/cloudwatch_alarm/graph.png",  # bucket url
                "alt_text": "cloudwatch image"
            }
        ]

        return content

    def result(self):
        return self.STAGE[self.status]()

