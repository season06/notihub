import boto3
import time

class DoAction(object):
    def __init__(self, credentials, info):
        self.credentials = credentials
        self.info = info

        self.ACTION = {}
        self.ACTION['elb'] = self.elb_action
        self.ACTION['ec2'] = self.ec2_action
        # self.ACTION['rds'] = self.elb_action

    def elb_action(self):
        elb_client = boto3.client(
            'elbv2',
            region_name=self.info['region'],
            aws_access_key_id=self.credentials['AccessKeyId'],
            aws_secret_access_key=self.credentials['SecretAccessKey'],
            aws_session_token=self.credentials['SessionToken'],
        )

        # from target group to find instance id
        response = elb_client.describe_target_health(
            TargetGroupArn=f"arn:aws:elasticloadbalancing:{self.info['region']}:{self.info['account_id']}:{self.info['target_group']}"
        )
        target_list = response['TargetHealthDescriptions']
        instance_id = 'none'

        for target in target_list:
            if target['TargetHealth']['State'] == "unhealthy":
                instance_id = target['Target']['Id']
                print(instance_id)
                break
        else:
            raise Exception('There is no unhealthy instance')

        # run command
        ssm_client = boto3.client(
            'ssm',
            region_name=self.info['region'],
            aws_access_key_id=self.credentials['AccessKeyId'],
            aws_secret_access_key=self.credentials['SecretAccessKey'],
            aws_session_token=self.credentials['SessionToken'],
        )
        response = ssm_client.send_command(
            InstanceIds=[instance_id],
            DocumentName="AWS-RunShellScript",
            Parameters={'commands': [self.info['command']]}
        )

        time.sleep(5)

        command_id = response['Command']['CommandId']
        output = ssm_client.get_command_invocation(
            CommandId=command_id,
            InstanceId=instance_id
        )

        print(output)

    def ec2_action(self):
        ec2_client = boto3.client(
            'ec2',
            region_name=self.info['region'],
            aws_access_key_id=self.credentials['AccessKeyId'],
            aws_secret_access_key=self.credentials['SecretAccessKey'],
            aws_session_token=self.credentials['SessionToken'],
        )

        # stop instance
        response = ec2_client.stop_instances(
            InstanceIds=[
                self.info['instance_id'],
            ],
        )

        # start instance
        while True:
            check = ec2_client.describe_instances(
                InstanceIds=[
                    self.info['instance_id'],
                ],)
            instance_status = check["Reservations"][0]["Instances"][0]["State"]["Code"]
            
            if instance_status == 80:  # stopped
                response = ec2_client.start_instances(
                    InstanceIds=[
                        self.info['instance_id'],
                    ],
                )
                print("EC2 has been started", "\n", response)
            elif instance_status == 16:  # running
                break

    def result(self):
        return self.ACTION[self.info['alarm_type']]()
